/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetjava2;

import panel.Home;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import panel.AjouterConferencePanel;
import panel.ListeConferencePanel;
import panel.ListeSalariePanel;

/**
 *
 * @author Utilisateur
 */
public class CustomWindow extends JFrame implements ActionListener {

    private static final String TITRE = "Application AD";
    private static final int WIDTH = 800;
    private static final int HEIGHT = 800;

    private static final boolean VISIB = true;
    private JMenuItem fermerSys;
    private JMenuItem AjouterConf;
    private JMenuItem revenirHome;
    private JMenuItem VoirListeConf;
    private JMenuItem VoirListeClient;

    public CustomWindow() {
        super(TITRE);

        this.setSize(WIDTH, HEIGHT);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


//creation de la bar de menu
        JMenuBar menuBar = new JMenuBar();
//creation dun onglet
        JMenu menuFichier = new JMenu("Fichier");
        menuBar.add(menuFichier);
        JMenu menuHome = new JMenu("Home");
        menuBar.add(menuHome);
        JMenu MenuConference = new JMenu("Conference");
        menuBar.add(MenuConference);
        JMenu MenuClient = new JMenu("Client");
        menuBar.add(MenuClient);
  

//ajouter bar de menue  a ma fenetre 
        this.setJMenuBar(menuBar);

//ajouter un sous-onglet a mon onglet principal appeler fichier
        revenirHome = new JMenuItem("Menu");
        menuHome.add(revenirHome);
        revenirHome.addActionListener(this);

        fermerSys = new JMenuItem("Fermer");
        menuFichier.add(fermerSys);
        
        VoirListeConf = new JMenuItem("Liste des conference");
        MenuConference.add(VoirListeConf);
        VoirListeConf.addActionListener(this);
        
        AjouterConf = new JMenuItem("Ajouter une conference");
        MenuConference.add(AjouterConf);
        AjouterConf.addActionListener(this);
        

        
        
        
   
        
        VoirListeClient = new JMenuItem("Voir Liste Client");
        MenuClient.add(VoirListeClient);
        VoirListeClient.addActionListener(this);

// rendre visble la bar de menu
        menuBar.setVisible(VISIB);
        fermerSys.addActionListener(this);
        
        Home panel = new Home();
        this.setContentPane(panel);
        this.repaint();
        this.revalidate();
        
        this.setVisible(VISIB);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == fermerSys) {
            System.exit(0);
            
        }
       else if (e.getSource() == VoirListeConf) {
           ListeConferencePanel panel2 = new ListeConferencePanel();
            this.setContentPane(panel2);
            this.repaint();
            this.revalidate();
        
        }
           else if (e.getSource() == revenirHome) {
           Home panel = new Home();
            this.setContentPane(panel);
            this.repaint();
            this.revalidate();
        
        }
                   else if (e.getSource() == AjouterConf) {
           AjouterConferencePanel panel3 = new AjouterConferencePanel();
            this.setContentPane(panel3);
            this.repaint();
            this.revalidate();
        
        }
         else if (e.getSource() == VoirListeClient) {
           ListeSalariePanel panel3 = new ListeSalariePanel();
            this.setContentPane(panel3);
            this.repaint();
            this.revalidate();
        
        }

    }

}
