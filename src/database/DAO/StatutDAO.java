/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.DAO;

import com.mysql.jdbc.Connection;
import static database.DatabaseUtilities.exec;
import static database.DatabaseUtilities.getConnexion;
import database.POJO.InscriptionSatut;
import database.POJO.Statut;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import panel.ListeConferencePanel;

/**
 *
 * @author Utilisateur
 */
public class StatutDAO {
       public static ArrayList<Statut> getAll() {
           
        ArrayList<Statut> lectureStatut = new ArrayList<>();
        try {

//DONNEES DEPUIS LA BDD
            String query = "SELECT * FROM Statut";
            ResultSet resultat = exec(query);
           
            while (resultat.next()) {
                int idStatut = resultat.getInt("idStatut");
                String  designationStatut = resultat.getString("designationStatut");
               
                int row = resultat.getRow();
                Statut c = new Statut(idStatut,designationStatut );
                lectureStatut.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeConferencePanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lectureStatut;
    }

    public static Statut get(int a) {
        try {

            String query = "SELECT * FROM Statut WHERE idStatut= " + a;
            ResultSet resultat = exec(query);
            Calendar datCal = Calendar.getInstance();
                while (resultat.next()) {
                int idStatut = resultat.getInt("idStatut");
                String  designationStatut = resultat.getString("designationStatut");
               
                int row = resultat.getRow();
                Statut c = new Statut(idStatut,designationStatut );
                return c;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeConferencePanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Statut insert(Statut a) {

        try {
            PreparedStatement ps = null;
            Connection maConnexion = getConnexion();
            String query = "INSERT INTO Statut"
                    + "(idStatut, designationStatut)" + "VALUES(?,?)";
            ps = maConnexion.prepareStatement(query);
            ps.setInt(0, a.getIdStatutS());
            ps.setString(1, a.getDesignationStatut());
  
            ps.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public static void delete(int e) {
        String query = "DELETE FROM Statut WHERE id= " + e;
        exec(query);
    }

    public static void update(Statut e) {

        String query = "UPDATE Statut SET  idStatut=?, designationStatut=? WHERE idStatut =  " + e;
        exec(query);

}
    
}
