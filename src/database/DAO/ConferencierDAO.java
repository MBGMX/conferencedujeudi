/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.DAO;

import com.mysql.jdbc.Connection;
import static database.DatabaseUtilities.exec;
import static database.DatabaseUtilities.getConnexion;
import database.POJO.Conference;
import database.POJO.ConferenceStatut;
import database.POJO.Conferencier;
import database.POJO.Salle;
import database.POJO.Theme;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import panel.ListeConferencePanel;

/**
 *
 * @author Utilisateur
 */
public class ConferencierDAO {

    
         public static ArrayList<Conferencier> getAll() {    
             
        ArrayList<Conferencier> lectureUneConf = new ArrayList<>();
        try {

//DONNEES DEPUIS LA BDD
            String query = "SELECT * FROM Conferencier";
            ResultSet resultat = exec(query);
            Calendar datCal = Calendar.getInstance();
            while (resultat.next()) {
                int idConferencier = resultat.getInt("idConferencier");
                String  nomPrenomConferencier = resultat.getString("nomPrenomConferencier");
                boolean conferencierInterne = resultat.getBoolean("conferencierInterne");
                int idConference = resultat.getInt("idConference");
                Conference conferenceS = ConferenceDAO.get(idConference);
                
                String  blocNoteConferencier = resultat.getString("blocNoteConferencier");
                int row = resultat.getRow();
                Conferencier c = new Conferencier(idConferencier,nomPrenomConferencier, conferencierInterne, conferenceS,blocNoteConferencier );
                lectureUneConf.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeConferencePanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lectureUneConf;
    }

    public static Conferencier get(int a) {
            Salle salleS = null;
            Theme themeS = null;
             Conference conferenceS = null;
        try {

            String query = "SELECT * FROM Conferencier WHERE idConferencier= " + a;
            ResultSet resultat = exec(query);
            Calendar datCal = Calendar.getInstance();
            while (resultat.next()) {
                int id = resultat.getInt("id");
                String nomPrenomConferencier = resultat.getString("nomPrenomConferencier");
                java.sql.Date date = resultat.getDate("Date");
                datCal.setTimeInMillis(date.getTime());
                 boolean conferencierInterne = resultat.getBoolean("conferencierInterne");
                int idSalle = resultat.getInt("idSalle");
                salleS = SalleDAO.get(idSalle);
                
                int idTheme = resultat.getInt("idTheme");
                themeS = ThemeDAO.get(idTheme);
                
                int idConference = resultat.getInt("idConference");
                conferenceS = ConferenceDAO.get(idConference);
                 String  blocNoteConferencier = resultat.getString("blocNoteConferencier");
                int row = resultat.getRow();

               Conferencier c = new Conferencier(id,nomPrenomConferencier, conferencierInterne,blocNoteConferencier );
               return c;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeConferencePanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static ConferenceStatut insert(Conferencier a) {

        try {
            PreparedStatement ps = null;
            Connection maConnexion = getConnexion();
            String query = "INSERT INTO Conferencier"
                    + "(nomPrenomConferencier ,conferencierInterne,idConferencer,locNoteConferencier )" + "VALUES(?,?,?,?)";
            ps = maConnexion.prepareStatement(query);
            ps.setString(1, a.getNomPrenomConferencier());
            ps.setBoolean(2,a.getConferencierInterne());
            ps.setInt(3,a.getConferencierS().getIdConference());
            ps.setString(4,a.getBlocNoteConferencier());
            ps.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public static void delete(int z) {
        String query = "DELETE FROM Conferencier WHERE id= " + z;
        exec(query);
    }

    public static void update(Conferencier e) {

        String query = "UPDATE Conferencier SET  nomPrenomConferencier=?, conferencierInterne=?, idConference=?, blocNoteConferencier = ? WHERE idConferencier =  " + e;
        exec(query);

    }
}
