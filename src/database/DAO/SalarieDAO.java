/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.DAO;

import com.mysql.jdbc.Connection;
import static database.DatabaseUtilities.exec;
import static database.DatabaseUtilities.getConnexion;
import database.POJO.InscriptionSatut;
import database.POJO.Salarie;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import panel.ListeConferencePanel;

/**
 *
 * @author Utilisateur
 */
public class SalarieDAO {
       public static ArrayList<Salarie> getAll() {
        ArrayList<Salarie> listeSalarie = new ArrayList<>();
        try {

//DONNEES DEPUIS LA BDD
            String query = "SELECT * FROM Salarie";
            ResultSet resultat = exec(query);
            Calendar datCal = Calendar.getInstance();
            while (resultat.next()) {
                int idSalarie = resultat.getInt("idSalarie");
                String  nomPrenomSalarie = resultat.getString("nomPrenomSalarie");

                int row = resultat.getRow();
                Salarie c = new Salarie(idSalarie,nomPrenomSalarie);
                listeSalarie.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeConferencePanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listeSalarie;
    }

    public static Salarie get(int a) {
        try {

            String query = "SELECT * FROM Salarie WHERE id= " + a;
            ResultSet resultat = exec(query);
           
            while (resultat.next()) {
                int idSalarie = resultat.getInt("idSalarie");
                String  nomPrenomSalarie = resultat.getString("nomPrenomSalarie");

                int row = resultat.getRow();
                Salarie c = new Salarie(idSalarie,nomPrenomSalarie);
                return c;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeConferencePanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Salarie insert(Salarie a) {

        try {
            PreparedStatement ps = null;
            Connection maConnexion = getConnexion();
            String query = "INSERT INTO Salarie"
                    + "(idSalarie, ,nomPrenomSalarie)" + "VALUES(?,?)";
            ps = maConnexion.prepareStatement(query);
            ps.setInt(0, a.getIdSalarie());
            ps.setString(1, a.getNomPrenomSalarie());
            ps.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public static void delete(int idSalaries) {
        String query = "DELETE FROM Salarie WHERE idSalarie= " + idSalaries;
        exec(query);
    }

    public static void update(Salarie idSalaries) {

        String query = "UPDATE Salarie SET  idSalarie=?, nomPrenomSalarie=? WHERE idSalarie =" + idSalaries ;
        exec(query);

}
    
}
