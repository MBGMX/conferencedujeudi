/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.DAO;

import com.mysql.jdbc.Connection;
import static database.DatabaseUtilities.exec;
import static database.DatabaseUtilities.getConnexion;
import database.POJO.InscriptionSatut;
import database.POJO.Statut;
import database.POJO.Theme;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import panel.ListeConferencePanel;

/**
 *
 * @author Utilisateur
 */
public class ThemeDAO {
        public static ArrayList<Theme> getAll() throws SQLException {
        ArrayList<Theme> lectureTheme = new ArrayList<>();
     
//DONNEES DEPUIS LA BDD
            String query = "SELECT * FROM Theme";
            ResultSet resultat = exec(query);
           
            while (resultat.next()) {
                int idTheme;
               
                    idTheme = resultat.getInt("idTheme");
                
                String  designationTheme = resultat.getString("designationTheme");
               
                int row = resultat.getRow();
                Theme c = new Theme(idTheme,designationTheme );
                lectureTheme.add(c);
            }
        
        return lectureTheme;
    }

    public static Theme get(int a) {
        try {

            String query = "SELECT * FROM Theme WHERE idTheme= " + a;
            ResultSet resultat = exec(query);

                while (resultat.next()) {
                int idTheme = resultat.getInt("idTheme");
                String  designationTheme = resultat.getString("designationTheme");
               
                int row = resultat.getRow();
                Theme c = new Theme(idTheme,designationTheme );
                return c;
            }
        }   catch (SQLException ex) { 
                Logger.getLogger(ThemeDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        return null;
    }

    public static Theme insert(Theme a) {

        try {
            PreparedStatement ps = null;
            Connection maConnexion = getConnexion();
            String query = "INSERT INTO Theme"
                    + "(idTheme, designationTheme)" + "VALUES(?,?)";
            ps = maConnexion.prepareStatement(query);
            ps.setInt(0, a.getIdTheme());
            ps.setString(1, a.getDesignationTheme());
  
            ps.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public static void delete(int e) {
        String query = "DELETE FROM Theme WHERE idTheme= " + e;
        exec(query);
    }

    public static void update(Theme z) {

        String query = "UPDATE Theme SET  idTheme=?, designationTheme=? WHERE idTheme =  "+z;
        exec(query);

}
}
