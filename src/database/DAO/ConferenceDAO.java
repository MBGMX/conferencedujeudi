/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.DAO;

import com.mysql.jdbc.Connection;
import static database.DatabaseUtilities.exec;
import static database.DatabaseUtilities.getConnexion;
import database.POJO.Conference;
import database.POJO.Conferencier;
import database.POJO.Salle;
import database.POJO.Theme;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import panel.ListeConferencePanel;

/**
 *
 * @author Utilisateur
 */
public class ConferenceDAO {

    public static ArrayList<Conference> getAll() {
        Conference conferenceS = null;
        Salle salleS = null;
        Theme themeS= null;
        Conferencier conferencierS = null;
        ArrayList<Conference> listConference = new ArrayList<>();
        try {
            

//DONNEES DEPUIS LA BDD
            String query = "SELECT * FROM Conference";
            ResultSet resultat = exec(query);
            Calendar datCal = Calendar.getInstance();
            while (resultat.next()) {
                int id = resultat.getInt("id");
             
                
                
                String titre = resultat.getString("Titre");
                java.sql.Date date = resultat.getDate("Date");
                datCal.setTimeInMillis(date.getTime());
                
                int idSalle = resultat.getInt("idSalle");
                salleS = SalleDAO.get(idSalle);
                
                int idTheme = resultat.getInt("idTheme");
                themeS = ThemeDAO.get(idTheme);
                
                int idConferencier = resultat.getInt("idConferencier");
                conferencierS = ConferencierDAO.get(idConferencier);
                
                int row = resultat.getRow();
                Conference c = new Conference(id, titre,datCal, conferencierS, salleS, themeS );
                listConference.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeConferencePanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listConference;
    }

    public static Conference get(int idConf) {
                Conference conferenceS = null;
        Salle salleS = null;
        Theme themeS= null;
        Conferencier conferencierS = null;
        try {

            String query = "SELECT * FROM conference WHERE id= " + idConf;
            ResultSet resultat = exec(query);
            Calendar datCal = Calendar.getInstance();
            while (resultat.next()) {
                int id = resultat.getInt("id");
                String titre = resultat.getString("Titre");
                java.sql.Date date = resultat.getDate("Date");
                datCal.setTimeInMillis(date.getTime());
                int idSalle = resultat.getInt("idSalle");
                salleS = SalleDAO.get(idSalle);
                
                int idTheme = resultat.getInt("idTheme");
                themeS = ThemeDAO.get(idTheme);
                
                int idConferencier = resultat.getInt("idConferencier");
                conferencierS = ConferencierDAO.get(idConferencier);
                
                int row = resultat.getRow();

                     Conference c = new Conference(id, titre,datCal,conferencierS, salleS, themeS );
                return c;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeConferencePanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Conference insert(Conference a) {

        try {
            PreparedStatement ps = null;
            Connection maConnexion = getConnexion();
            String query = "INSERT INTO Conference"
                    + "(Titre,Date,idSalle,idTheme,idConferencier)" + "VALUES(?,?,?,?,?)";
            ps = maConnexion.prepareStatement(query);
            ps.setString(1, a.geTtitreConference());
            ps.setDate(2, a.getSqlDate());
            ps.setInt(5, a.getConferencierS().getIdConferencier());
            ps.setInt(3, a.getSalleS().getIdsalle());
            ps.setInt(4, a.getThemeS().getIdTheme());
            ps.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public static void delete(int idConf) {
        String query = "DELETE FROM conference WHERE id= " + idConf;
        exec(query);
    }

    public static void update(Conference conf) {

        String query = "UPDATE conference SET  titre=?, date=?, idSalle=?, idTheme=? WHERE id = idConf ";
        exec(query);

    }

}
