/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.DAO;

import com.mysql.jdbc.Connection;
import static database.DatabaseUtilities.exec;
import static database.DatabaseUtilities.getConnexion;
import database.POJO.Conference;
import database.POJO.ConferenceStatut;
import database.POJO.Conferencier;
import database.POJO.Inscription;
import database.POJO.InscriptionSatut;
import database.POJO.Statut;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import panel.ListeConferencePanel;

/**
 *
 * @author Utilisateur
 */
public class InscriptionStatutDAO {
       public static ArrayList<InscriptionSatut> getAll() {
        ArrayList<InscriptionSatut> lectureInscriStatut = new ArrayList<>();
        try {

//DONNEES DEPUIS LA BDD
            String query = "SELECT * FROM InscriptionStatut";
            ResultSet resultat = exec(query);
            Calendar datCal = Calendar.getInstance();
            while (resultat.next()) {
                int idInscription = resultat.getInt("idInscription");
                Inscription inscriptionS = InscriptionDAO.get(idInscription);
                
                 int idStatut = resultat.getInt("idStatut");
                 Statut statutS = StatutDAO.get(idStatut);
               java.sql.Date dateStatutInscription = resultat.getDate("dateStatutInscription");
                datCal.setTimeInMillis(dateStatutInscription.getTime());


                int row = resultat.getRow();
                InscriptionSatut c = new InscriptionSatut(inscriptionS,statutS, dateStatutInscription);
                lectureInscriStatut.add(c);
                return lectureInscriStatut;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeConferencePanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lectureInscriStatut;
    }

    public static InscriptionSatut get(int d) {
        try {

            String query = "SELECT * FROM InscriptionStatut WHERE id= " + d;
            ResultSet resultat = exec(query);
            Calendar datCal = Calendar.getInstance();
                       while (resultat.next()) {
               int idInscription = resultat.getInt("idInscription");
                Inscription inscriptionS = InscriptionDAO.get(idInscription);
                
                 int idStatut = resultat.getInt("idStatut");
                 Statut statutS = StatutDAO.get(idStatut);
               java.sql.Date dateStatutInscription = resultat.getDate("dateStatutInscription");
                datCal.setTimeInMillis(dateStatutInscription.getTime());


                int row = resultat.getRow();
                InscriptionSatut c = new InscriptionSatut(inscriptionS,statutS, dateStatutInscription);
                return c;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeConferencePanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static InscriptionSatut insert(InscriptionSatut a) {

        try {
            PreparedStatement ps = null;
            Connection maConnexion = getConnexion();
            String query = "INSERT INTO InscriptionStatut"
                    + "(idInscription, idStatut,dateStatutInscription)" + "VALUES(?,?,?)";
            ps = maConnexion.prepareStatement(query);
            ps.setInt(1, a.getInscriptionS().getIdInscription());
            ps.setInt(2, a.getStatutS().getIdStatutS());
            ps.setDate(3, a.getDateSatutInscription());
            ps.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public static void delete(int a) {
        String query = "DELETE FROM InscriptionStatut WHERE id= " + a;
        exec(query);
    }

    public static void update(InscriptionSatut a) {

        String query = "UPDATE InscriptionStatut SET  idInscription=?, idStatut=?, dateSatutInscription=? WHERE idInscription = "+a ;
        exec(query);

}
}