/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.DAO;

import com.mysql.jdbc.Connection;
import static database.DatabaseUtilities.exec;
import static database.DatabaseUtilities.getConnexion;
import database.POJO.Conference;
import database.POJO.ConferenceStatut;
import database.POJO.Conferencier;
import database.POJO.Inscription;
import database.POJO.Salarie;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import panel.ListeConferencePanel;

/**
 *
 * @author Utilisateur
 */
public class InscriptionDAO {
       public static ArrayList<Inscription> getAll() throws SQLException {
           Salarie salarieS = null;
           Conference conferenceS = null;
        ArrayList<Inscription> lectureInscris = new ArrayList<>();
      
//DONNEES DEPUIS LA BDD
            String query = "SELECT * FROM Inscription";
            ResultSet resultat = exec(query);
            Calendar datCal = Calendar.getInstance();
            while (resultat.next()) {
                int idInscription = resultat.getInt("idInscription");
                int  idSalarie = resultat.getInt("idSalarie");
                salarieS = SalarieDAO.get(idSalarie);
                 int  idConference = resultat.getInt("idConference");
                 conferenceS = ConferenceDAO.get(idConference);
                
                int row = resultat.getRow();
                Inscription c = new Inscription(idInscription,salarieS, conferenceS);
                lectureInscris.add(c);
            }
        
        return lectureInscris;
    }

    public static Inscription get(int e) throws SQLException {
         
     
            String query = "SELECT * FROM Inscription WHERE idInscription= " + e;
            ResultSet resultat = exec(query);
            Calendar datCal = Calendar.getInstance();
          while (resultat.next()) {
                int idInscription = resultat.getInt("idInscription");
                int  idSalarie = resultat.getInt("idSalarie");
                Salarie salarieS = SalarieDAO.get(idSalarie);
                 int  idConference = resultat.getInt("idConference");
                 Conference conferenceS = ConferenceDAO.get(idConference);
                
                int row = resultat.getRow();
                Inscription c = new Inscription(idInscription,salarieS, conferenceS);
             
                return c;
            }
        
        return null;
    }

    public static Inscription insert(Inscription a) {

        try {
            PreparedStatement ps = null;
            Connection maConnexion = getConnexion();
            String query = "INSERT INTO Inscription"
                    + "(idInscription,idSalarie, idConference)" + "VALUES(?,?,?)";
            ps = maConnexion.prepareStatement(query);
            ps.setInt(1, a.getIdInscription());
            ps.setInt(2, a.getSalarieS().getIdSalarie());
            ps.setInt(3, a.getConferenceS().getIdConference());
            ps.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public static void delete(int e) {
        String query = "DELETE FROM Inscription WHERE id= " + e;
        exec(query);
    }

    public static void update(Inscription r) {

        String query = "UPDATE Inscription SET  idInscription=?, idSalarie=?, idConference=? WHERE idInscription =  " + r;
        exec(query);

}}
