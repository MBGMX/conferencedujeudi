/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.DAO;

import com.mysql.jdbc.Connection;
import static database.DatabaseUtilities.exec;
import static database.DatabaseUtilities.getConnexion;
import database.POJO.InscriptionSatut;
import database.POJO.Salle;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import panel.ListeConferencePanel;

/**
 *
 * @author Utilisateur
 */
public class SalleDAO {
       public static ArrayList<Salle> getAll() {
        ArrayList<Salle> lectureUneConf = new ArrayList<>();
        try {

//DONNEES DEPUIS LA BDD
            String query = "SELECT * FROM Salle";
            ResultSet resultat = exec(query);

            while (resultat.next()) {
                int idSalle = resultat.getInt("idSalle");
                String  nomSalle = resultat.getString("nomSalle");
                    int nbPlaceSalle = resultat.getInt("nbPlaceSalle");
                int row = resultat.getRow();
                Salle c = new Salle(idSalle,nomSalle, nbPlaceSalle);
                lectureUneConf.add(c);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeConferencePanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lectureUneConf;
    }

    public static Salle get(int a) {
        try {

            String query = "SELECT * FROM Salle WHERE idSalle= " + a;
            ResultSet resultat = exec(query);
            Calendar datCal = Calendar.getInstance();
  while (resultat.next()) {
                int idSalle = resultat.getInt("idSalle");
                String  nomSalle = resultat.getString("nomSalle");
                    int nbPlaceSalle = resultat.getInt("nbPlaceSalle");
                int row = resultat.getRow();
                Salle c = new Salle(idSalle,nomSalle, nbPlaceSalle);
                return c;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListeConferencePanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Salle insert(Salle a) {

        try {
            PreparedStatement ps = null;
            Connection maConnexion = getConnexion();
            String query = "INSERT INTO Salle"
                    + "(idSalle, nomSalle,nbPlaceSalle)" + "VALUES(?,?,?)";
            ps = maConnexion.prepareStatement(query);
            ps.setInt(0, a.getIdsalle());
            ps.setString(1, a.getNomSalle());
            ps.setInt(2, a.getNbPlaceSalle());
            ps.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public static void delete(int a) {
        String query = "DELETE FROM Salle WHERE id= " + a;
        exec(query);
    }

    public static void update(Salle a) {

        String query = "UPDATE Salle SET  idSalle=?, nomSalle=?, nbPlaceSalle=? WHERE idSalle = " + a;
        exec(query);

}
    
}
