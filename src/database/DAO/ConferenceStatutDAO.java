/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.DAO;

import com.mysql.jdbc.Connection;
import static database.DatabaseUtilities.exec;
import static database.DatabaseUtilities.getConnexion;
import database.POJO.Conference;
import database.POJO.ConferenceStatut;
import database.POJO.Statut;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;


/**
 *
 * @author Utilisateur
 */
public class ConferenceStatutDAO {
    
     public static ArrayList<ConferenceStatut> getAll() throws SQLException {
         Conference conferenceS;
         Statut statutS;
         
        ArrayList<ConferenceStatut> lectureStatutConf = new ArrayList<>();
       

//DONNEES DEPUIS LA BDD
            String query = "SELECT * FROM conferenceStatut";
            ResultSet resultat = exec(query);
            Calendar datCal = Calendar.getInstance();
            while (resultat.next()) {
                int idConference = resultat.getInt("idConference");
                conferenceS = ConferenceDAO.get(idConference);
                
                int  idStatut = resultat.getInt("idStatut");
                statutS = StatutDAO.get(idStatut);
                
                java.sql.Date date = resultat.getDate("dateStatutConference");
                datCal.setTimeInMillis(date.getTime());
                int row = resultat.getRow();
                ConferenceStatut c = new ConferenceStatut(conferenceS, statutS, date);
                lectureStatutConf.add(c);
            }
        
        return lectureStatutConf;
    }

    public static ConferenceStatut get(int z) throws SQLException {
              Conference conferenceS =null;
         Statut statutS = null;
         
        

            String query = "SELECT * FROM conferenceStatut WHERE id= " + z;
            ResultSet resultat = exec(query);
            Calendar datCal = Calendar.getInstance();
            while (resultat.next()) {
                  int idConference = resultat.getInt("idConference");
                conferenceS = ConferenceDAO.get(idConference);
                
                int  idStatut = resultat.getInt("idStatut");
                statutS = StatutDAO.get(idStatut);
                
                java.sql.Date date = resultat.getDate("dateStatutConference");
                datCal.setTimeInMillis(date.getTime());
                int row = resultat.getRow();
                ConferenceStatut c = new ConferenceStatut(conferenceS, statutS, date);
                return c;
            
        }
        return null;
    }

    public static ConferenceStatut insert(ConferenceStatut a) {

        try {
            PreparedStatement ps = null;
            Connection maConnexion = getConnexion();
            String query = "INSERT INTO conferenceStatut"
                    + "(idConference, idStatut,dateStatutConference)" + "VALUES(?,?,?)";
            ps = maConnexion.prepareStatement(query);
            ps.setInt(1, a.getConferenceS().getIdConference());
            ps.setInt(2, a.getStatutS().getIdStatutS());
            ps.setDate(2, a.getDateSatutConference());
            ps.execute();
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        return null;
    }

    public static void delete(int u) {
        String query = "DELETE FROM conferenceStatut WHERE id= " + u;
        exec(query);
    }

    public static void update(Conference k) {

        String query = "UPDATE conferenceStatut SET  idConference=?, idStatut=?, dateStatutConference=? WHERE idConference =  "+k;
        exec(query);

    }
    
}
