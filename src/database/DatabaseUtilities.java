/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import com.mysql.jdbc.Connection;
import static java.lang.Class.forName;
import java.sql.DriverManager;
import static java.sql.DriverManager.getConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Utilisateur
 */
public class DatabaseUtilities {

    public static Connection getConnexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        Connection connection = null;

        try {
            connection = (Connection) DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/ConferenceDuJeudi", "root", "root");
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return connection;
    }

    public static ResultSet exec(String query) {
        Connection maConnexion = getConnexion();

        Statement statement = null;
        try {
            statement = maConnexion.createStatement();
            if (statement.execute(query)) {

                return statement.getResultSet();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseUtilities.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
