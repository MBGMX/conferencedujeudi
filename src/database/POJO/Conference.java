/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.POJO;

import com.mysql.jdbc.Connection;
import static database.DatabaseUtilities.getConnexion;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class Conference {

    private int idConference;
    private String titreConference;
    private Calendar dateConference;
       private Date dateConferences;
    private Salle salleS;
    private Theme themeS;
    private Conferencier conferencierS;
    private String nomConferencier;
    private Date sqlDate;

    public Conference() {

    }

    public Conference(int _idConference, String _titreConference, Conferencier _idConferencier) {
        this.idConference = _idConference;
        this.titreConference = _titreConference;
        this.conferencierS = _idConferencier;

    }

    public Conference(int _idConference, String _titreConference, Conferencier _idConferencier, Date _dateConference, Salle _idSalle, Theme _idTheme) {
        this.idConference = _idConference;
        this.titreConference = _titreConference;
        this.conferencierS = _idConferencier;
        this.dateConferences = _dateConference;
        this.salleS = _idSalle;
        this.themeS = _idTheme;
    

    }
    
        public Conference( String _titreConference, Calendar _dateConference,Conferencier _idConferencier, Salle _idSalle, Theme _idTheme) {
       
        this.titreConference = _titreConference;
        this.dateConference = _dateConference;
        this.conferencierS = _idConferencier;

        this.salleS = _idSalle;
        this.themeS = _idTheme;

    }

    public Conference(int _id, String _titreConference, java.sql.Date _dateConference, Conferencier _idConferencier, Salle _idSalle, Theme _idTheme) {
        this.titreConference = _titreConference;
        this.idConference = _id;
        this.conferencierS = _idConferencier;
        this.sqlDate = _dateConference;
        this.salleS = _idSalle;
        this.themeS = _idTheme;
      
}
    
      public Conference(int _id, String _titreConference, Calendar _dateConference, Conferencier _idConferencier, Salle _idSalle, Theme _idTheme) {
        this.titreConference = _titreConference;
        this.idConference = _id;
        this.conferencierS = _idConferencier;
        this.dateConference = _dateConference;
        this.salleS = _idSalle;
        this.themeS = _idTheme;
      
}

    public Conference(Integer idC, String titre, Calendar datCal, Integer Sallej, Integer Sallei, Integer Salley) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
  


    public int getIdConference() {
        return this.idConference;
    }

    public void setIdConference(int _idConference) {
        this.idConference = _idConference;
    }
    
    

    public String geTtitreConference() {
        return this.titreConference;
    }

    public void settitreConference(String _titreConference) {
        this.titreConference = _titreConference;
    }

    public Conferencier getConferencierS() {
        return this.conferencierS;
    }

    public void setConferencierS(Conferencier _idConferencier) {
        this.conferencierS = _idConferencier;
    }

    public Calendar getdateConference() {

        return this.dateConference;

    }

    public String getDateString() {
        Calendar datCal = this.dateConference;
        SimpleDateFormat formate = new SimpleDateFormat("dd-MM-yyyy");
        String formated = formate.format(datCal.getTime());
        return formated;
    }

    public void setidConferencier(Calendar _dateConference) {
        this.dateConference = _dateConference;
    }

    public Salle getSalleS() {
        return this.salleS;
    }

    public void setSalleS(Salle _idSalle) {
        this.salleS = _idSalle;
    }

    public Theme getThemeS() {
        return this.themeS;
    }

    public void setThemeS(Theme _idTheme) {
        this.themeS = _idTheme;
    }





    public java.sql.Date getSqlDate() {
java.sql.Date dateSql = new java.sql.Date(dateConference.getTimeInMillis());
return dateSql;
    }

}
