/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.POJO;

/**
 *
 * @author Utilisateur
 */
public class Statut {
    private int idStatut;
    private String designationStatut;
    
    public Statut(int _idStatut, String _designationStatut)
    {
        this.idStatut = _idStatut;
        this.designationStatut=_designationStatut;
    }
    
    public int getIdStatutS()
    {
        return this.idStatut;
    }
    public void setIdStatutS( int _idStatut)
    {
        this.idStatut = _idStatut;
    }
    
    
    public String getDesignationStatut()
    {
        return this.designationStatut;
    }
    public void setDesignationStatut(String _designationStatut)
    {
        this.designationStatut = _designationStatut;
    }
}
