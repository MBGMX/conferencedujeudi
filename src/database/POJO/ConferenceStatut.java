/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.POJO;

import java.sql.Date;
import java.util.Calendar;

/**
 *
 * @author Utilisateur
 */
public class ConferenceStatut {
    private Conference conferenceS;
    private Statut statutS;
    private Date dateStatutConference;
    
    public ConferenceStatut (Conference _idConference, Statut _idStatut, Date _dateSatutConference)
    {
        this.conferenceS = _idConference;
        this.statutS = _idStatut;
        this.dateStatutConference= _dateSatutConference;
    }

 
    
    public Conference getConferenceS()
    {
        return this.conferenceS;
    }
    public void setConferenceS( Conference _idConference)
    {
        this.conferenceS= _idConference;
    }
    
        public Statut getStatutS()
    {
        return this.statutS;
    }
    public void setStatutS( Statut _idStatut)
    {
        this.statutS= _idStatut;
    }
    public Date getDateSatutConference()
    {
        return this.dateStatutConference;
    }
    public void setDateStatutConference(Date _dateStatutConference)
    {
        this.dateStatutConference = _dateStatutConference;
    }
    
}
