/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.POJO;

/**
 *
 * @author Utilisateur
 */
public class Inscription {
    private int idInscription;
    private Salarie salarieS;
    private Conference conferenceS;
    
    public Inscription( int _idInscription, Salarie _idSalle, Conference _idConference)
    {
        this.idInscription = _idInscription;
        this.salarieS = _idSalle;
        this.conferenceS=_idConference;
    }
    public int getIdInscription()
    {
        return this.idInscription;
    }
    public void setIdInscription(int _idInscription)
    {
        this.idInscription= _idInscription;
    }
    public Salarie getSalarieS()
    {
        return this.salarieS;
    }
    public void setIdSalarieS( Salarie _idSalarie)
    {
        this.salarieS = _idSalarie;
    }
    public Conference getConferenceS()
    {
        return this.conferenceS;
    }
    public void setConferenceS(Conference _idConference)
    {
        this.conferenceS = _idConference;
    }




}
