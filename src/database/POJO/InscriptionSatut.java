/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.POJO;

import java.sql.Date;
import java.util.Calendar;

/**
 *
 * @author Utilisateur
 */
public class InscriptionSatut {
    private Inscription idInscription;
    // private int idStatut;
    private Statut statutS;
    private Date dateSatutInscription;
    
    public InscriptionSatut(Inscription _idInscription, Statut _statut, Date _dateSatutInscription)
    {
        this.idInscription=_idInscription;
        this.statutS=  _statut;
        this.dateSatutInscription = _dateSatutInscription;
    }


    public Inscription getInscriptionS()
    {
        return this.idInscription;
    }
    public void setInscriptionS( Inscription _idInscription)
    {
        this.idInscription = _idInscription;
    }
    
        public Statut getStatutS()
    {
        return this.statutS;
    }
    public void setIdStatut( Statut _s)
    {
        this.statutS = _s;
    }
    public Date getDateSatutInscription()
    {
        return this.dateSatutInscription;
    }
    public void setDateSatutInscription( Date _dateSatutInscription)
    {
        this.dateSatutInscription = _dateSatutInscription;
    }
}
