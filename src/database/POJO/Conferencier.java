/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database.POJO;

/**
 *
 * @author Utilisateur
 */
public class Conferencier {
    private int idConferencier;
    private String nomPrenomConferencier;
    private boolean conferencierInterne;
    private Conference conferenceS;
    private String blocNoteConferencier;
    
    
        public Conferencier()
    {
        
    }
     public Conferencier(int _idConferencier, String _nomPrenomConferencier, Conference _idConference)
    {
        this.idConferencier = _idConferencier;
        this.nomPrenomConferencier = _nomPrenomConferencier;
        this.conferenceS=_idConference;
    }
    
    public Conferencier(int _idConferencier, String _nomPrenomConferencier, boolean _conferencierInterne, String _blocNoteConferencier)
    {
        this.idConferencier = _idConferencier;
        this.nomPrenomConferencier = _nomPrenomConferencier;
        this.conferencierInterne=_conferencierInterne;
      
        this.blocNoteConferencier=_blocNoteConferencier;
    }
     public Conferencier( String _nomPrenomConferencier, boolean _conferencierInterne,  String _blocNoteConferencier)
    {
     
        this.nomPrenomConferencier = _nomPrenomConferencier;
        this.conferencierInterne=_conferencierInterne;
         
        this.blocNoteConferencier=_blocNoteConferencier;
    }
    
    public int getIdConferencier()
    {
      return this.idConferencier;  
    }
    public void SetIdConferencier(int _idConferencier)
    {
        this.idConferencier = _idConferencier;
    }
    
    
    
     public String getNomPrenomConferencier()
    {
      return this.nomPrenomConferencier;  
    }
    public void SetIdConferencier(String _nomPrenomConferencier)
    {
        this.nomPrenomConferencier = _nomPrenomConferencier;
    }
    
    
    public boolean getConferencierInterne()
    {
      return this.conferencierInterne;  
    }
    public void setConferencierInterne(boolean _conferencierInterne)
    {
        this.conferencierInterne = _conferencierInterne;
    }
    
    
        public Conference getConferencierS()
    {
      return this.conferenceS;  
    }
    public void setConferencierS(Conference _idConference)
    {
        this.conferenceS = _idConference;
    }
    
    
    public String getBlocNoteConferencier()
    {
        return this.blocNoteConferencier;
    }
    public void setBlocNoteConferencier(String _blocNoteConferencier)
    {
        this.blocNoteConferencier = _blocNoteConferencier;
    }
    
    
    
}
