/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import java.awt.List;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import database.POJO.Conference;
import java.awt.event.ActionListener;

/**
 *
 * @author Utilisateur
 */
public class ConferenceModele extends AbstractTableModel {

    //Le tableau contenant la liste 
    private String[] columnsName = {"titre", "date", "Conference"}; //Les entêtes du tableau


    private ArrayList<Conference> listConference = new ArrayList<>();

    public ConferenceModele(ArrayList<Conference> _listeConference) {
        this.listConference = _listeConference;
    }



    @Override
    public int getColumnCount() {
        return columnsName.length;
    }

    @Override

    public int getRowCount() {

        return listConference.size();
    }

    public String getColumnName(int columnIndex) {
        return columnsName[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Conference maConference = listConference.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return maConference.geTtitreConference();
            case 1:
                return maConference.getDateString();
            case 2:
                return maConference.getIdConference();
            default:
                return null;
        }
    }

    public int getSelectedRow() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }




}
