/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import database.POJO.Conference;
import database.POJO.Salarie;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Utilisateur
 */
public class SalarieModele extends AbstractTableModel{
      //Le tableau contenant la liste 
    private String[] columnsName = {"idClient", "Nom Prenom Client"}; //Les entêtes du tableau


    private ArrayList<Salarie> listeSalarie = new ArrayList<>();

    public SalarieModele(ArrayList<Salarie> _listeSalarie) {
        this.listeSalarie = _listeSalarie;
    }



    @Override
    public int getColumnCount() {
        return columnsName.length;
    }

    @Override

    public int getRowCount() {

        return listeSalarie.size();
    }

    public String getColumnName(int columnIndex) {
        return columnsName[columnIndex];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Salarie mesClient = listeSalarie.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return mesClient.getIdSalarie();
            case 1:
                return mesClient.getNomPrenomSalarie();
               
        } return null;
    }

    public int getSelectedRow() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
 
}
