/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panel;

import static com.sun.java.accessibility.util.AWTEventMonitor.addActionListener;
import database.DAO.ConferenceDAO;
import database.DatabaseUtilities;
import static database.DatabaseUtilities.exec;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import modele.ConferenceModele;
import database.POJO.Conference;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import projetjava2.ProjetJava2;

/**
 *
 * @author Utilisateur
 */
public class ListeConferencePanel extends JPanel {

    Box principalBox = Box.createVerticalBox();
    Box box2 = Box.createHorizontalBox();
    Box box3 = Box.createHorizontalBox();
    Box box4 = Box.createHorizontalBox();

    public ListeConferencePanel() {

        JLabel label = new JLabel("Liste des Conférences");
        box2.add(label);
        principalBox.add(box2);
        label.setLocation(500, 50);
        label.setForeground(Color.BLUE);
        Font font = new Font("KacstTitle", Font.ITALIC + Font.BOLD, 62);
        label.setFont(font);
        JButton btn_test = new JButton("Test");
        box2.add(btn_test);


        //DONNEES EN DUR
           Calendar datCal = Calendar.getInstance();
//        datCal.set(1989, 11, 23);
//        Conference c1 = new Conference(1, "La belle Dormeuse", 1, datCal, 123, 5, "jacque");
//        Conference c2 = new Conference(1, "Le Beau Dormeur", 1, datCal, 123, 5, "Higor");
//        Conference c3 = new Conference(1, "Le Belle au bois Dormant", 1, datCal, 123, 5, "Grichor");
//        ArrayList<Conference> listConference = new ArrayList<>();
//        listConference.add(c1);
//        listConference.add(c2);
//        listConference.add(c3);
        ArrayList<Conference> listConference = ConferenceDAO.getAll();//j'appel ma class ConferenceDAO LA ou j'ai creer la methode qui me permet d'afficher la liste via la methode getAlll
      

        ConferenceModele ConferenceModele = new ConferenceModele(listConference);
        JTable table = new JTable(ConferenceModele);
        int row = table.getSelectedRow(); 
 
        table.setSize(
                400, 400);
        table.setAutoCreateRowSorter(
                true);
        JScrollPane scrollpane = new JScrollPane(table);


        box4.add(scrollpane);

        principalBox.add(box4);

        this.add(principalBox);

        box4.setVisible(true);

   
     
       btn_test.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, row);
   
             }
        });
                
                
          
  
        
        
    }
      
     
}
