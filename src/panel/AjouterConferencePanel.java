/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panel;

import database.DAO.ConferenceDAO;
import database.DAO.ConferencierDAO;
import database.POJO.Conference;
import database.POJO.Conferencier;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import static java.sql.DriverManager.println;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Utilisateur
 */
public class AjouterConferencePanel extends JPanel {

    Box principalBox = Box.createVerticalBox();
    Box box2 = Box.createHorizontalBox();
    Box box2a = Box.createVerticalBox();
    Box boxForm = Box.createVerticalBox();
    Box box3 = Box.createHorizontalBox();
    Box box3a = Box.createHorizontalBox();
    Box box3b = Box.createHorizontalBox();
    Box box3c = Box.createHorizontalBox();
    Box box3d = Box.createHorizontalBox();
    Box box3e = Box.createHorizontalBox();
    Box box4 = Box.createHorizontalBox();

    public AjouterConferencePanel() {

        JLabel label = new JLabel("Bienvenue");
        box2.add(label);

        this.setBackground(Color.decode("#6495ED"));//papier peint
        principalBox.add(box2);
        label.setLocation(500, 50);
        label.setForeground(Color.BLUE);
        Font font = new Font("KacstTitle", Font.ITALIC + Font.BOLD, 62);
        label.setFont(font);

         JTextField idConference = new JTextField();
        JLabel titreChampDeSaisies = new JLabel("Id de la Conference");
        box3.add(titreChampDeSaisies);
        idConference.setColumns(10); //On lui donne un nombre de colonnes à afficher
        box3.add(idConference);
        idConference.setForeground(Color.BLUE);
        idConference.setBackground(Color.white);
        
        JTextField TitreConf = new JTextField();
        JLabel titreChampDeSaisie = new JLabel("Titre de la Conference");
        box3.add(titreChampDeSaisie);
        TitreConf.setColumns(10); //On lui donne un nombre de colonnes à afficher
        box3.add(TitreConf);
        TitreConf.setForeground(Color.BLUE);
        TitreConf.setBackground(Color.white);

        JTextField dateConf = new JTextField();
        JLabel titreChampDeSaisie2 = new JLabel("Date de la Conference");
        box3a.add(titreChampDeSaisie2);
        dateConf.setColumns(10); //On lui donne un nombre de colonnes à afficher
        box3a.add(dateConf);
        dateConf.setForeground(Color.BLUE);
        dateConf.setBackground(Color.white);
        boxForm.add(box3a);

        JTextField idConf = new JTextField();
        JLabel titreChampDeSaisie3 = new JLabel("id de la Conference");
        box3b.add(titreChampDeSaisie3);
        idConf.setColumns(10); //On lui donne un nombre de colonnes à afficher
        box3b.add(idConf);
        idConf.setForeground(Color.BLUE);
        idConf.setBackground(Color.white);
        boxForm.add(box3b);

        JTextField idSall = new JTextField();
        JLabel titreChampDeSaisie4 = new JLabel("id de la Salle Conference");
        box3c.add(titreChampDeSaisie4);
        idSall.setColumns(10); //On lui donne un nombre de colonnes à afficher
        box3c.add(idSall);
        idSall.setForeground(Color.BLUE);
        idSall.setBackground(Color.white);
        boxForm.add(box3c);

        JTextField idTheme = new JTextField();
        JLabel titreChampDeSaisie5 = new JLabel("id du Theme de la conference");
        box3d.add(titreChampDeSaisie5);
        idTheme.setColumns(10); //On lui donne un nombre de colonnes à afficher
        box3d.add(idTheme);
        idTheme.setForeground(Color.BLUE);
        idTheme.setBackground(Color.white);
        boxForm.add(box3d);

        JButton bouttonVal = new JButton("Valider");
        box3e.add(bouttonVal);
        bouttonVal.setForeground(Color.BLUE);
        bouttonVal.setBackground(Color.RED);
        boxForm.add(box3e);

        this.setBackground(Color.decode("#6495ED"));//papier peint

        JLabel label2 = new JLabel("");
        box3.add(label2);;
        principalBox.add(box3);
        principalBox.add(boxForm);
        this.add(principalBox);

        bouttonVal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String id = idConference.getText();
                Integer idC;
                idC = Integer.parseInt(id);
                String titre = TitreConf.getText();
                String date = dateConf.getText();
                String conferencier = idConf.getText();
                String Salle = idSall.getText();
                String Theme = idTheme.getText();
                Integer sallei;
                Integer themeK;
                Integer conferencierK;
                sallei = Integer.parseInt(Salle);
                themeK = Integer.parseInt(Theme);
                
                conferencierK = Integer.parseInt(conferencier);
                Conferencier c = ConferencierDAO.get(conferencierK);

              Calendar datCal = Calendar.getInstance();
                Conference conf = new Conference( titre, datCal, c,sallei, themeK);
                if (titre.length() < 1)
                {
                    
                   JOptionPane.showMessageDialog(null, "Le champ Titre et requis", "Important", 0);
                  
                }
               else if (Salle.length() < 1)
                {
                    JOptionPane.showMessageDialog(null, "Le champ Salle et requis", "Important", 0);
                  
                }
               else if (Theme.length() < 1)
                {
                    JOptionPane.showMessageDialog(null, "Le champ Theme et requis", "Important", 0);
                    
                }
                else
                {
                ConferenceDAO.insert(conf);
                
                TitreConf.setText("");
                dateConf.setText("");
                idConf.setText("");
                idSall.setText("");
                idTheme.setText("");
                JOptionPane.showMessageDialog(null, "Enregistrement effectuer");
                }
                
                
            }
        });
    }
}
