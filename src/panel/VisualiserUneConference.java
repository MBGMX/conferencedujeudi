/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panel;

import database.DAO.ConferenceDAO;
import database.POJO.Conference;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import modele.ConferenceModele;

/**
 *
 * @author Utilisateur
 */
public class VisualiserUneConference extends JPanel{
     Box principalBox = Box.createVerticalBox();
    Box box2 = Box.createHorizontalBox();
    Box box3 = Box.createHorizontalBox();
    Box box4 = Box.createHorizontalBox();

    public VisualiserUneConference() {

        JLabel label = new JLabel("Votre Conférences");
        box2.add(label);
        principalBox.add(box2);
        label.setLocation(500, 50);
        label.setForeground(Color.BLUE);
        Font font = new Font("KacstTitle", Font.ITALIC + Font.BOLD, 62);
        label.setFont(font);
        JButton btn_test = new JButton("Test");
        box2.add(btn_test);
        

        //DONNEES EN DUR
        Calendar datCal = Calendar.getInstance();
        ArrayList<Conference> listConference = ConferenceDAO.getAll();//j'appel ma class ConferenceDAO LA ou j'ai creer la methode qui me permet d'afficher la liste via la methode getAlll
      

        ConferenceModele ConferenceModele = new ConferenceModele(listConference);
        JTable table = new JTable(ConferenceModele);
        int row = table.getSelectedRow(); 
 
        table.setSize(
                400, 400);
        table.setAutoCreateRowSorter(
                true);
        JScrollPane scrollpane = new JScrollPane(table);


        box4.add(scrollpane);

        principalBox.add(box4);

        this.add(principalBox);

        box4.setVisible(true);

}}
